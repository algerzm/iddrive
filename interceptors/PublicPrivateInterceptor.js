import { getBasicAuthorization } from '../services'
import { getValidationErrorMessage } from '../utilities'
import axios from 'axios'

export const PublicPrivateInterceptor = () => {
  axios.interceptors.request.use(function (request) {
    if (request.headers?.Authorization) {
      return request
    }
    const token = getBasicAuthorization()
    const newHeaders = {
      Authorization: token,
      'Content-Type': 'application/json',
    }

    request.headers = newHeaders
    return request
  })

  axios.interceptors.response.use(
    (response) => {
      window.location.href === '/reset-success'
      return response
    },
    function (error) {
      console.log(error)
      let messageError = getValidationErrorMessage(error)
      //Then i can show a snackbar
      //snackbar method here with messageError
    }
  )
}
