/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  compiler: {
    styledComponents: true,
  },
  images: {
    domains: [
      'cdn.icon-icons.com',
      'assets.stickpng.com',
      'www.adverthia.com',
      'logos-marcas.com',
      'logos-world.net',
      'upload.wikimedia.org',
    ],
  },
}

module.exports = nextConfig
