import styled from 'styled-components'

export const LayoutContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 29px;
`

export const Heading = styled.div`
  flex-direction: row;
  box-sizing: border-box;
`
