import * as Yup from 'yup'

const appFormSchema = Yup.object().shape(
  {
    name: Yup.string()
      .required('App Name is required')
      .min(3, 'App Name must be at least 3 characters'),
    url: Yup.string().required('App URL is required').url('App URL must be a valid URL'),
    user: Yup.string()
      .nullable()
      .notRequired()
      .when('user', {
        is: (user) => user?.length,
        then: (rule) => rule.min(3, 'User must be at least 3 characters'),
      })
      .when('email', {
        is: (email) => !email?.length,
        then: (rule) => rule.required('You must provide an username or email'),
      }),
    email: Yup.string().email('Email must be a valid email'),
    password: Yup.string()
      .nullable()
      .notRequired()
      .when('password', {
        is: (password) => password?.length,
        then: (rule) => rule.min(2, 'Password must be at least 2 characters'),
      }),
    hint: Yup.string().optional(),
    description: Yup.string()
      .nullable()
      .notRequired()
      .when('description', {
        is: (description) => description?.length,
        then: (rule) => rule.min(5, 'Description must be at least 5 characters'),
      }),
  },
  [
    ['user', 'user'],
    ['description', 'description'],
    ['password', 'password'],
    ['user', 'email'],
  ]
)

export { appFormSchema }
