export const getValidationErrorMessage = (error) => {
  switch (error) {
    default:
      return 'Something went wrong!'
  }
}
