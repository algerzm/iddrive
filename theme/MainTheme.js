import { createTheme } from '@mui/material/styles'

const MainTheme = createTheme({
  palette: {
    primary: {
      main: '#311e69',
    },
    secondary: {
      main: '#1C1C4A',
    },
    terciary: {
      main: '#EFBB45',
    },
  },
})

export { MainTheme }
