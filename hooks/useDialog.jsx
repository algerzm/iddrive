import { useState } from 'react'
import { Dialog, Paper } from '@mui/material'

export default function useDialog({ RenderComponent, onCloseCallback, onOpenCallback, maxWidth }) {
  const [open, setOpen] = useState(false)

  const handleClose = () => {
    onCloseCallback && onCloseCallback()
    setOpen(false)
  }
  const handleClickOpen = () => {
    onOpenCallback && onOpenCallback()
    setOpen(true)
  }

  const DialogComponent = () => (
    <Dialog onClose={handleClose} open={open} fullWidth maxWidth={maxWidth || 'md'}>
      {RenderComponent ? RenderComponent : 'Oops, something went wrong'}
    </Dialog>
  )

  return [handleClickOpen, DialogComponent]
}
