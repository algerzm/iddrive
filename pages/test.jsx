import axios from 'axios'
import React, { useState, useEffect } from 'react'

export default function Test() {
  const [user, setUser] = useState(null)
  useEffect(() => {
    if (!user) {
      async function getUser() {
        let nUser = await axios.post(url)
        setUser(nUser)
      }

      getUser()
    }
  })

  return (
    <div>
      <h1>{user ? user : 'loading'}</h1>
    </div>
  )
}
