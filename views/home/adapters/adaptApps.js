export const createAdaptedApps = (apps) => {
  const adaptedApps = []

  apps.forEach((app) => {
    const { id, name, image, url, user, email, password, hint, description, lastUpdate } = app
    const adaptApp = {
      id,
      name,
      image,
      url,
      user,
      email,
      password,
      hint,
      description,
      lastUpdate,
    }
    adaptedApps.push(adaptApp)
  })

  return adaptedApps
}
