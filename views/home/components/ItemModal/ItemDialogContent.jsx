import { Typography, Grid } from '@mui/material'
import Image from 'next/image'
import ItemForm from './ItemForm'

import { useSelector } from 'react-redux'

export default function ItemDialogContent() {
  const appsState = useSelector((store) => store.apps)
  const { selectedApp } = appsState

  return (
    <Grid container>
      <Grid
        item
        xs={12}
        sm={4}
        sx={{
          width: '100%',
          height: 'auto',
          padding: '1rem',
          backgroundColor: '#311e69',
          borderRadius: 0,
        }}
      >
        <Grid id='draggable-side-panel' container>
          <Grid item xs={12} style={{ margin: 'auto' }}>
            <div style={{ maxWidth: '100px', margin: 'auto' }}>
              <Image
                width={1}
                height={1}
                layout='responsive'
                src={selectedApp.image || '/home/add-new-item.jpg'}
                alt='social media icon'
              ></Image>
            </div>
          </Grid>
          <Grid item xs={12} sx={{ paddingTop: '1rem' }}>
            <Typography variant='h4' color='white' textAlign='center'>
              {selectedApp.name || 'New App'}
            </Typography>
            <Typography
              variant='body1'
              color='white'
              sx={{ marginTop: '1rem', textAlign: 'center', marginBottom: '1rem' }}
            >
              {selectedApp.description ||
                'Lets add a new app to your list! \n Just fill out the form below.'}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} sm={8}>
        <ItemForm app={selectedApp} />
      </Grid>
    </Grid>
  )
}
