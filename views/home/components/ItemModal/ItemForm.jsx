import React, { useEffect, useState } from 'react'
import { Paper, Grid, TextField, Button } from '@mui/material'
import PasswordTextField from '../../../../components/PasswordTextField'
import InputTextField from '../../../../components/InputTextField'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { useSelector } from 'react-redux'

import { appFormSchema } from '../../../../utilities/Validations/appForm.schema'

export default function ItemForm() {
  const [isEditing, setIsEditing] = useState(false)
  const appsState = useSelector((store) => store.apps)
  const { selectedApp } = appsState
  const formOptions = {
    resolver: yupResolver(appFormSchema),
    mode: 'onChange',
  }

  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
    setValue,
  } = useForm(formOptions)

  useEffect(() => {
    if (selectedApp.name) {
      setValue('name', selectedApp.name)
      setValue('description', selectedApp.description)
      setValue('user', selectedApp.user)
      setValue('password', selectedApp.password)
      setValue('hint', selectedApp.hint)
      setValue('url', selectedApp.url)
      setValue('email', selectedApp.email)
    }
  }, [selectedApp, setValue])

  const onSubmit = (data) => console.log(data)
  return (
    <Paper
      component='form'
      sx={{ width: '100%', height: '100%', padding: '1rem' }}
      onSubmit={handleSubmit(onSubmit)}
    >
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <InputTextField
            name='name'
            label='App Name'
            defaultValue={''}
            register={register}
            error={errors.name}
            helperText={errors.name?.message}
            required
            disabled={!isEditing && selectedApp.name}
          />
        </Grid>
        <Grid item xs={12}>
          <InputTextField
            name='url'
            label='App URL'
            defaultValue={''}
            register={register}
            error={errors.url}
            helperText={errors.url?.message}
            disabled={!isEditing && selectedApp.name}
            required
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <InputTextField
            name='user'
            label='User Name'
            defaultValue={''}
            register={register}
            error={errors.user}
            disabled={!isEditing && selectedApp.name}
            helperText={errors.user?.message}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <InputTextField
            name='email'
            label='Email'
            defaultValue={''}
            register={register}
            error={errors.email}
            disabled={!isEditing && selectedApp.name}
            helperText={errors.email?.message}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <InputTextField
            name='hint'
            label='Password Hint'
            defaultValue={''}
            register={register}
            error={errors.hint}
            disabled={!isEditing && selectedApp.name}
            helperText={errors.hint?.message}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <PasswordTextField
            register={register}
            error={errors.password}
            helperText={errors.password?.message}
            disabled={!isEditing && selectedApp.name}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            label='Description'
            multiline
            rows={2}
            variant='outlined'
            disabled={!isEditing && selectedApp.name}
            fullWidth
            {...register('description')}
            error={Boolean(errors.description)}
            helperText={errors.description?.message}
          ></TextField>
        </Grid>
        <Grid item xs={12} textAlign='right'>
          {isEditing ? (
            <>
              <Button
                variant='contained'
                sx={{ marginRight: '1rem' }}
                onClick={() => setIsEditing(false)}
                color='error'
              >
                Cancel
              </Button>
              <Button type='submit' variant='contained'>
                Save
              </Button>
            </>
          ) : selectedApp.name ? (
            <Button variant='contained' onClick={() => setIsEditing(true)} color='warning'>
              Modify
            </Button>
          ) : (
            <Button variant='contained' type='submit'>
              Create
            </Button>
          )}
        </Grid>
      </Grid>
    </Paper>
  )
}
