import { Fragment } from 'react'
import Image from 'next/image'
import { Typography } from '@mui/material'

export default function NoAppsRegistered() {
  return (
    <Fragment>
      <Image loading='eager' width={400} height={270} src='/home/no-data-purple.png' alt='no-data-found' />
      <Typography variant='h6' color='primary'>
        Oops! No Apps found, Let&apos;s create a new one!
      </Typography>
    </Fragment>
  )
}
