import { Collapse, Avatar, Divider } from '@mui/material'
import Image from 'next/image'

import { useState } from 'react'
import styles from '../styles/SideBar.module.css'

import SettingsIcon from '@mui/icons-material/Settings'
import LogoutIcon from '@mui/icons-material/Logout'
import TranslateIcon from '@mui/icons-material/Translate'
import ShieldIcon from '@mui/icons-material/Shield'
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts'

//*Components and Styled Components
import {
  SidePanel,
  IconsGroupBox,
  LogoBox,
  SidePanelContainer,
  SignOutButton,
} from '../styled-components/SidePanel'
import IconItem from './IconItem'

export default function SideNavBar() {
  const [open, setOpen] = useState(true)
  return (
    <SidePanelContainer>
      <LogoBox>
        <Image width={80} height={80} src='/home/logo-full-white.png' alt='usb-drawing' />
      </LogoBox>
      <SidePanel>
        <div>
          <Avatar
            onClick={() => setOpen(!open)}
            sx={{ width: 50, height: 50 }}
            className={styles.avatar}
            src='https://www.freecodecamp.org/news/content/images/2021/03/Quincy-Larson-photo.jpg'
          />
          <Divider sx={{ marginTop: '1rem' }}></Divider>
          <Collapse in={open} timeout={200} unmountOnExit>
            <IconsGroupBox>
              <IconItem Icon={ManageAccountsIcon} tooltip='Manage Account' />
              <IconItem Icon={ShieldIcon} tooltip='Security' />
              <IconItem Icon={TranslateIcon} tooltip='Language' />
              <IconItem Icon={SettingsIcon} tooltip='Settings' />
            </IconsGroupBox>
          </Collapse>
        </div>
        <SignOutButton aria-label='sign-out' color='secondary'>
          <LogoutIcon sx={{ fontSize: '2rem' }} />
        </SignOutButton>
      </SidePanel>
    </SidePanelContainer>
  )
}
