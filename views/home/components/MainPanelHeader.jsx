import { TextField, InputAdornment } from '@mui/material'

import SearchIcon from '@mui/icons-material/Search'

export default function MainPanelHeader({ searchFunction }) {
  return (
    <TextField
      InputProps={{
        startAdornment: (
          <InputAdornment position='start'>
            <SearchIcon />
          </InputAdornment>
        ),
      }}
      fullWidth
      onChange={searchFunction}
    />
  )
}
