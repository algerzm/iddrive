import { Typography, Avatar } from '@mui/material'
import { AppItemGrid } from '../styled-components/Home'
import styles from '../styles/AppItem.module.css'

//* Store
import { useDispatch } from 'react-redux'
import { selectApp } from '../../../redux/states/apps'

export default function AppItem({ app, openDialog }) {
  const { name, image } = app
  const dispatch = useDispatch()

  const handleClick = () => {
    dispatch(selectApp(app))
    openDialog()
  }

  return (
    <AppItemGrid item xs={6} sm={3} md={2}>
      <div className={styles.itemContainer} onClick={() => handleClick()}>
        <Avatar
          variant='square'
          sx={{ width: 55, height: 55 }}
          src={image}
          imgProps={{
            className: styles.image,
          }}
        />
        <Typography variant='body1' sx={{ marginTop: '0.3rem' }}>
          {name}
        </Typography>
      </div>
    </AppItemGrid>
  )
}
