import { IconButton, Tooltip } from '@mui/material'

export default function IconItem({ Icon, tooltip }) {
  return (
    <Tooltip title={tooltip} placement='right'>
      <IconButton
        aria-label='fingerprint'
        color='secondary'
        height={60}
        width={60}
        sx={{
          marginTop: '1rem',
        }}
      >
        <Icon />
      </IconButton>
    </Tooltip>
  )
}
