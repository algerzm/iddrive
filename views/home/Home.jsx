import { useEffect } from 'react'
import { Grid, Fab, Tooltip } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
//*Store
import { useDispatch, useSelector } from 'react-redux'
import { setAppsList, filterAppsByName, clearSelectedApp } from '../../redux/states/apps'
// * Styled Components
import {
  TopColorDiv,
  MainPanel,
  SidePanelGrid,
  MainPanelGrid,
  NoAppsGrid,
  AppItemsGrid,
} from './styled-components/HomeLayout'
// * Components
import SideNavBar from './components/SideNavBar'
import MainPanelHeader from './components/MainPanelHeader'
import NoAppsRegistered from './components/NoAppsRegistered'
import AppItem from './components/AppItem'
import ItemDialogContent from './components/ItemModal/ItemDialogContent'
//*Services
import { getApps } from './services/app.service'
//* Adapters
import { createAdaptedApps } from './adapters/adaptApps'
//* Hooks
import useDialog from '../../hooks/useDialog'

export default function Home() {
  const dispatch = useDispatch()

  const [openDialog, AppItemDialog] = useDialog({
    RenderComponent: <ItemDialogContent />,
    onCloseCallback: () => dispatch(clearSelectedApp()),
  })

  const appsState = useSelector((store) => store.apps)
  const { appsList: apps } = appsState

  useEffect(() => {
    const fetchApps = async () => {
      const apps = await getApps()
      console.log(apps)
      dispatch(setAppsList(createAdaptedApps(apps)))
    }
    fetchApps()
  }, [dispatch])

  const renderAppItems = () => {
    return apps.map((app) => <AppItem key={app.id} app={app} openDialog={openDialog} />)
  }

  const searchApps = (e) => {
    const searchTerm = e.target.value
    if (searchTerm.match(/^[a-zA-Z0-9_.-]*$/)) {
      dispatch(filterAppsByName(searchTerm.toLowerCase()))
    }
  }

  return (
    <>
      <TopColorDiv />
      <AppItemDialog />
      <Grid container spacing={2}>
        <SidePanelGrid item xs={3} sm={2} md={2} lg={1}>
          <SideNavBar />
        </SidePanelGrid>
        <MainPanelGrid item xs={9} sm={10} md={10} lg={11}>
          <MainPanel>
            <Grid container sx={{ height: apps.length === 0 && '100%' }}>
              <Grid item xs={12} sx={{ height: 'auto' }}>
                <MainPanelHeader searchFunction={searchApps} />
              </Grid>
              {apps.length > 0 ? (
                <AppItemsGrid item xs={12}>
                  <Grid container>{renderAppItems()}</Grid>
                </AppItemsGrid>
              ) : (
                <NoAppsGrid item xs={12}>
                  <NoAppsRegistered />
                </NoAppsGrid>
              )}

              <Tooltip title='Add new app' placement='left-start'>
                <Fab
                  color='primary'
                  aria-label='add'
                  sx={{ position: 'absolute', bottom: 80, right: 80 }}
                  onClick={() => openDialog()}
                >
                  <AddIcon fontSize='large' />
                </Fab>
              </Tooltip>
            </Grid>
          </MainPanel>
        </MainPanelGrid>
      </Grid>
    </>
  )
}
