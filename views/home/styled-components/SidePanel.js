import { Box, IconButton } from '@mui/material'
import styled from 'styled-components'

export const SidePanel = styled.section`
  height: 80%;
  width: 80%;
  background-color: #fff;
  border-radius: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  display: flex;
  padding-top: 1rem;
  padding-bottom: 1rem;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  z-index: 2;
`

export const IconsGroupBox = styled(Box)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-content: center;
  width: 100%;
`

export const LogoBox = styled.div`
  width: 80%;
  height: 100px;
  background-color: #311e69;
  height: 10%;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  z-index: 2;
`
export const SidePanelContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  align-items: center;
  justify-content: center;
  margin-left: 1rem;
  max-width: 110px;
`

export const SignOutButton = styled(IconButton)`
  margin-top: 1rem;
  width: 50px;
  height: 50px;
`
