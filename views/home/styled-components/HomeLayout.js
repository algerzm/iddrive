import { Grid } from '@mui/material'
import styled from 'styled-components'

export const MainPanel = styled.section`
  height: 90%;
  width: 100%;
  margin-right: 2.5rem;
  background-color: #fff;
  border-radius: 10px;
  padding: 2rem;
  z-index: 1;
`
export const SidePanelGrid = styled(Grid)`
  min-height: 100vh;
  margin-top: 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const MainPanelGrid = styled(Grid)`
  min-height: 100vh;
  margin-top: 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const NoAppsGrid = styled(Grid)`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  flex-direction: column;
  gap: 2rem;
  margin-top: -2rem;
`
export const AppItemsGrid = styled(Grid)`
  display: flex;
  padding-top: 2rem;
`

export const TopColorDiv = styled.div`
  background-color: #f8b518;
  height: 20%;
  width: 100%;
  position: absolute;
  z-index: 0;
  top: 0;
`
