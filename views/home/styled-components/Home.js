import { Grid } from '@mui/material'
import styled from 'styled-components'

export const AppItemGrid = styled(Grid)`
  display: flex;
  justify-content: center;
  align-content: center;
`
