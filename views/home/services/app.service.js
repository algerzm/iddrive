import axios from 'axios'

export const getApps = async () => {
  const response = await axios.get('http://localhost:3000/api/apps')
  const apps = response.data.data

  return apps
}
