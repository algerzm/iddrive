import styled from 'styled-components'
import { Typography } from '@mui/material'

export const LoginFormBox = styled.section`
  width: 70%;
  height: 60%;
  background-color: white;
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  align-content: space-between;
  justify-content: space-between;
  min-height: 550px;
  min-width: 430px;
`

export const LoginHeader = styled.header`
  text-align: center;
  font-family: 'LilitaOne';
  font-size: 3rem;
`

export const LoginSubtitle = styled(Typography)`
  margin-top: -2.7rem;
`

export const LoginFormFields = styled.form`
  width: 100%;
  padding: 1rem;
  margin-top: 2rem;
  padding-left: 2rem;
  padding-right: 2rem;
  height: auto;
  display: flex;
  flex-direction: column;
  gap: 2rem;
`

export const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: row;
  gap: 10px;
  margin-top: 1rem;
`

export const LoginFooter = styled.footer`
  margin-top: 1rem;
  padding-left: 2rem;
  padding-right: 2rem;
  margin-bottom: 2rem;
  text-align: center;
`
