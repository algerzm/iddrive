import { TextField, Button, Divider, Typography } from '@mui/material'

// * Components
import {
  LoginFormBox,
  LoginHeader,
  LoginSubtitle,
  LoginFormFields,
  ButtonsContainer,
  LoginFooter,
} from '../styled-components/LoginComponents'
import InputTextField from '../../../components/InputTextField'

export default function LoginForm() {
  return (
    <LoginFormBox>
      <div>
        <LoginHeader>
          <p>Welcome Back...</p>
          <LoginSubtitle variant='subtitle1'>Please enter your email and password</LoginSubtitle>
        </LoginHeader>
        <LoginFormFields>
          <InputTextField label='E-Mail' />
          <TextField label='Password' fullWidth type='password' />
          <ButtonsContainer>
            <Button color='primary' fullWidth variant='contained'>
              Lets Go!
            </Button>
            <Button
              sx={{
                width: '60%',
              }}
              variant='contained'
              color='terciary'
            >
              Forgot Password
            </Button>
          </ButtonsContainer>
        </LoginFormFields>
      </div>
      <div>
        <Divider variant='middle' />
        <LoginFooter>
          <Typography variant='body2'>
            Dont have an account yet? Sign Up{' '}
            <a
              href='#'
              style={{
                color: 'blue',
                textDecoration: 'underline',
              }}
            >
              Here
            </a>
          </Typography>
        </LoginFooter>
      </div>
    </LoginFormBox>
  )
}
