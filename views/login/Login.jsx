import React from 'react'
import styles from './styles/Login.module.css'
import Image from 'next/image'

// * Components
import LoginForm from './components/LoginForm'

export default function Login() {
  return (
    <div className={styles.mainContainer}>
      <div className={styles.imageContainer}>
        <div className={styles.innerImageContainer}>
          <Image src='/login/PersonPurple.png' width={850} height={850} layout='intrinsic' alt='web security' />
        </div>
      </div>
      <main className={styles.loginFormContainer}>
        <LoginForm />
      </main>
    </div>
  )
}
