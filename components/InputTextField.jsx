import { TextField } from '@mui/material'

export default function InputTextField({
  label,
  defaultValue,
  register,
  error,
  name,
  helperText,
  required,
  disabled,
}) {
  return (
    <TextField
      label={label}
      defaultValue={defaultValue}
      fullWidth
      {...register(name)}
      error={Boolean(error)}
      helperText={helperText}
      variant='outlined'
      required={required}
      disabled={disabled}
    />
  )
}
