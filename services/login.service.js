import axios from 'axios'

const apiUrl = 'http://localhost:4000/'
const loginUrl = apiUrl + 'login'

export const login = (email, password) => {
  return axios.post(loginUrl, { email, password })
}
