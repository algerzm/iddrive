import { createSlice } from '@reduxjs/toolkit'

export const AppsEmptyState = {
  appsList: [],
  originalAppsList: [],
  appsListLoading: false,
  appsListError: false,
  selectedApp: {},
  selectedAppLoading: false,
  creatingApp: false,
}

export const appsSlice = createSlice({
  name: 'apps',
  initialState: AppsEmptyState,
  reducers: {
    selectApp: (state, action) => {
      state.selectedApp = action.payload
    },
    clearSelectedApp: (state, action) => {
      state.selectedApp = {}
    },
    modifyAppList: (state, action) => {
      state.appsList = [...state, action.payload]
    },
    setAppsList: (state, action) => {
      state.appsList = action.payload
      state.originalAppsList = action.payload
    },
    filterAppsByName: (state, action) => {
      state.appsList = state.originalAppsList.filter((app) => app.name.toLowerCase().includes(action.payload))
    },
  },
})

export const { selectApp, modifyAppList, setAppsList, setOriginalAppsList, filterAppsByName, clearSelectedApp } =
  appsSlice.actions

export default appsSlice.reducer
