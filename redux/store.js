import { configureStore } from '@reduxjs/toolkit'
import { appsSlice } from './states/apps'

export default configureStore({
  reducer: {
    apps: appsSlice.reducer,
  },
})
